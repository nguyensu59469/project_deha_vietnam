@extends("admin.Layouts.app")
@section("content")
    <div class="container-fluid">

        <!-- start page title -->
        @if(session('message'))
            <div>
                <h4 class="text-primary">{{ session('message') }}</h4>
            </div>
        @endif

        <div style="padding-left: 1%">
            <h1>Categories List</h1>
            <div>
                <a href="{{route('categories.create')}}" class="btn btn-primary">Create Categories</a>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12" style="text-align: center"
            >
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">

                            <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
                                <thead class="table-light">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>

                                    <th style="width: 85px;text-align: center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($Categories) != 0)
                                    @foreach($Categories as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>

                                            <td>
                                                <a href="{{route('categories.edit', $item->id)}}"
                                                   class="btn btn-success">Edit</a>
                                                <a href="{{ route('categories.show', $item->id) }}"
                                                   class="btn btn-warning">Show</a>
                                                <form action="{{ route('categories.destroy', $item->id) }}" class="delete-form"
                                                      method="post" style="display: inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button data-action="{{ route('categories.destroy', $item->id) }}"
                                                            class="btn btn-danger btn-delete">Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                @else
                                    <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center"><span
                                                style="font-size: 25px; color: #d8d8d8">No data...</span></td>
                                    </tr>
                                    </tbody>
                                @endif
                            </table>
                            {{ $Categories->appends(request()->all())->links() }}
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div><br><br>
    @push('scripts')
        <script src="{{asset('js/user.js')}}"></script>
    @endpush
@endsection
