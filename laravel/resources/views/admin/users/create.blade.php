@extends("admin.Layouts.app")
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Create User</h4>
                </div>
            </div>
        </div>
        <div>
            <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control">

                    @error('name')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Email</label>
                    <input type="text" value="{{old('email')}}" name="email" class="form-control">

                    @error('email')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Phone</label>
                    <input type="int" value="{{old('phone')}}" name="phone" class="form-control">

                    @error('phone')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Address</label>
                    <textarea value="{{old('address')}}" name="address" class="form-control"></textarea>

                    @error('address')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Gender</label>
                    <input type="text" value="{{old('gender')}}" name="gender" class="form-control">
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option value="male">Male</option>
                        <option value="female">Female</option>

                    </select>

                    @error('gender')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Password</label>
                    <input type="password" value="{{old('password')}}" name="password" class="form-control">

                    @error('password')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>
                <div class="mb-3">
                    <label class="form-label">Role</label>
                    <br>
                    <select class="form-select" name="role_ids[]" multiple size="5">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{ $role->display_name }}</option>
                        @endforeach
                    </select>

                </div>
                <div>
                    <a type="button" href="{{ route("users.index") }}" class="btn btn-secondary btn-button">Back</a>
                    <button type="submit" class="btn btn-primary btn-button">Create</button>
                </div>

            </form>
        </div>
    </div>
@endsection

