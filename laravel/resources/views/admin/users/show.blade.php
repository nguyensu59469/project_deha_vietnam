@extends("admin.Layouts.app")
@section("content")
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-md-4" style="margin: auto;padding: 10px;text-align: center">
                <h2 class="page-title">Detail user {{ $user->name }}</h2>

            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-md-12" style="margin: auto">
                <div class="card">
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-4">Id :</dt>
                            <dd class="col-sm-8">{{ $user->id }}</dd>
                            <dt class="col-sm-4">Name :</dt>
                            <dd class="col-sm-8">{{ $user->name }}</dd>
                            <dt class="col-sm-4">Email address :</dt>
                            <dd class="col-sm-8">{{ $user->email }}</dd>
                            <dt class="col-sm-4">Phone :</dt>
                            <dd class="col-sm-8">{{ $user->phone }}</dd>
                            <dt class="col-sm-4">Role :</dt>
                            <dd class="col-sm-8">
                                @if($user->hasRoles('super-admin'))
                                    <div class="col-sm-2">
                                        <span class="badge bg-success">Super Admin</span>
                                    </div>
                                @else
                                    @foreach($user->roles as $item)
                                        <div class="col-sm-2">
                                            <span class="badge bg-success">{{ $item->display_name }}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </dd>
                        </dl>
                    </div>

                </div>
                <div><br>
                    <a type="button" href="{{ url()->previous() }}" class="btn btn-primary btn-button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
