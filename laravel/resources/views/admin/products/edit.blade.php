<div id="modal-edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div style="margin: 20px 20px 20px 20px;">
                <h1 id="title-edit">Edit Product: </h1>

                <div id="product-id" data-url=""></div>
                <form id="updateProductForm" enctype="multipart/form-data" data-action="">

                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Name </label>
                        <input type="text" name="name" class="form-control" id="name-edit">
                        <div class="text-danger edit-error"><span id="name-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Price </label>
                        <input name="price" class="form-control" id="price-edit">
                        <div class="text-danger edit-error"><span id="price-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Description</label>
                        <input type="text" name="description" class="form-control" id="description-edit">
                        <div class="text-danger edit-error"><span id="description-error"></span></div>
                    </div>
                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Quantity</label>
                        <input type="text" name="quantity" class="form-control" id="quantity-edit">
                        <div class="text-danger edit-error"><span id="quantity-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Image </label><br>
                        <img class="rounded bg-light image-data img-thumbnail"
                             style="max-width: 30%; height: auto;" id="product-image-edit" src=""/>
                        <input accept="image/*" type="file" name="image" class="form-control" placeholder="Post Image"
                               id="image-edit">
                        <div class="text-danger edit-error"><span id="image-error"></span></div>
                    </div>

                    <div class="mb-3">
                        <label style="font-weight: bold" class="form-label">Category</label>
                        <select class="form-select" name="category_ids[]" id="category-edit" multiple>
                            @foreach ($categories as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div>
                        <a style="" class="btn btn-secondary" id="close-edit">Back</a>
                        <button id="btn-update" class="btn btn-primary btn-update" data-url="">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



