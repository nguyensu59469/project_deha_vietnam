@extends('admin.Layouts.app')
@section('content')
    <br>

    @if (session('message'))
        <div class="alert alert-primary" role="alert">
            {{ session('message') }}
        </div>
    @endif

    <div>
        <button type="button" class="btn btn-dribbble mb-2" id="btn-add-new-product"
                data-bs-toggle="modal" data-bs-target="#modal-form-create">
            <i class="mdi mdi-plus-circle me-1"></i>
            Create
        </button>
    </div>
    <h1>List Product</h1>

    <br>
    <div class="form-search" style="font-weight: bold">
        <form method="get" id="search-product">
            <div class="mb-3" style="display:flex">
                <label style="font-weight: bold" class="form-label">Name&emsp;</label>

                <input value="{{request()->name ?? ''}}" placeholder="Name" type="text" name="name"
                       class="form-control"
                       style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
            </div>
            <div class="mb-3" style="display:flex">

                <label style="font-weight: bold" class="form-label">Price&emsp;</label>

                <input value="{{request()->min_price ?? ''}}" placeholder="Min Price" name="min_price"
                       step="10000" min="0" class="form-control"
                       style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">

                <input value="{{request()->max_price ?? ''}}" placeholder="Max Price" name="max_price"
                       step="10000" min="0" class="form-control"
                       style="  padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
            </div>
            <div class="mb-3" style="display:flex">

                <label style="font-weight: bold" class="form-label">Category</label>

                <div style="margin-left: 20px; width: 94%">
                    <select  class="form-select category" name="category" type="text" id="select2" multiple
                             style="background-color: #fff; border: 1px solid #e7ebf0;">

                        @foreach($categories as $category)
                            <option
                                value="{{$category->id}}" {{ request()->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <button class="btn btn-success search-product" type="submit">Search</button>

        </form>
        <div>

            <br>
            <div class="row" id="show-product" data-action-table="{{route('products.list')}}"></div>

        </div>
    </div>
    <hr>

    @include('admin.products.show')
    @include('admin.products.create')
    @include('admin.products.edit')

@endsection

@push('scripts')
    <script src="{{asset('js/product.js')}}"></script>
@endpush


