<div class="container-fluid">

    <!-- start page title -->
    @if(session('message'))
        <div>
            <h4 class="text-primary">{{ session('message') }}</h4>
        </div>
    @endif
    <!-- end page title -->

    <div class="row">
        <div class="col-12" style="text-align: center">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">

                        <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
                            <thead class="table-light">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th style="width: 85px;text-align: center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($products) != 0)
                                @foreach($products as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>

                                        <td>
                                            <a class="btn btn-facebook btn-edit"
                                               data-url-edit="{{ route('products.edit', $item->id) }}">Edit</a>
                                            <a class="btn btn-warning btn-show"
                                               data-url-show="{{ route('products.show', $item->id) }}">Show</a>

                                            <button data-action="{{ route('products.destroy', $item->id) }}"
                                                    class="btn btn-danger btn-delete">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            @else
                                <tbody>
                                <tr>
                                    <td colspan="5" class="text-center"><span style="font-size: 25px; color: #d8d8d8">No data...</span>
                                    </td>
                                </tr>
                                </tbody>
                            @endif
                        </table>
                        {{ $products->appends(request()->all())->links() }}
                    </div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col -->
    </div>

</div>


