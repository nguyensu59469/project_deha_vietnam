<?php

namespace App\Http\Requests\User;

use App\Http\Rules\ValidateEmailRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => [
                'bail',
                'required',
                'unique:users',
                'email',
                new ValidateEmailRule($this->email)
            ],
            'gender' => 'required',
            'password' => 'required',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name cannot be null.',
            'email.max' => 'Emails cannot be longer than 255 characters.',
            'email.required' => 'Email cannot be null.',
            'email.unique' => 'Email already in use.',
            'password.required' => 'Password cannot be null.',
            'address.required' => 'Address cannot be null.',
            'phone.required' => 'Phone cannot be null.',
            'gender.required' => 'gender cannot be null.',
        ];
    }
}
