<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateEmailRule implements Rule
{

    protected $name;
    protected $email = 'deha-soft.com';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $domain = explode("@", $value)[1];
        return $domain === $this->email;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute domain must be @deha-soft.com .';
    }
}
