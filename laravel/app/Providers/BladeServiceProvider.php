<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //bao ve phan FE
        Blade::if('hasRole', function ($value) {
            return auth()->check() && (auth()->user()->hasRoles($value) || auth()->user()->isSupperAdmin());
        });
    }
}
