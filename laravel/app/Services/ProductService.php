<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['category_ids'] = $request->category_ids ?? null;
        $dataSearch['min_price'] = $request->min_price ?? null;
        $dataSearch['max_price'] = $request->max_price ?? null;

        return $this->productRepository->search($dataSearch);
    }

    public function all()
    {
        return $this->productRepository->latest('id')->paginate(5);
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->saveImage($request);
        $product = $this->productRepository->create($dataCreate);
        $product->attachCategory($request->category_ids);
        return $product;
    }

    public function create($request)
    {

        $dataCreate = $request->all();
        $dataCreate['category_ids'] = $dataCreate['category_ids'] ?? [];

        $dataCreate['image'] = $this->saveImage($request);
        $product = $this->productRepository->create($dataCreate);
        $product->syncCategories($dataCreate['category_ids']);
        return $product;
    }


    public function update($request, $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['image'] = $this->updateImage($request, $product->getRawOriginal('image'));
        $product->update($dataUpdate);
        $product->syncCategory($request->category_ids);
        return $product;
    }

    public function destroy($id)
    {
        $product = $this->productRepository->findOrFail($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }
}
