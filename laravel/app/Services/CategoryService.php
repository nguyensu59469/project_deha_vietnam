<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->latest('id')->paginate(5);
    }

    public function findOrFail($id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['slug'] = ($request->name);
        $dataCreate['parent_id'] = $request->parent_id ?? null;
        return $this->categoryRepository->create($dataCreate);
    }

    public function update($request, $id)
    {
        $cate = $this->categoryRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['parent_id'] = $request->parent_id ?? null;
        $cate->update($dataUpdate);
        return $cate;
    }

    public function destroy($id)
    {
        $cate = $this->categoryRepository->findOrFail($id);
        $cate->delete();
        return $cate;
    }
}
