<?php

namespace App\Services;

use App\Http\Requests\User\UpdateUserRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->latest('id')->paginate(5);
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['email'] = $request->email ?? null;
        $dataSearch['role_id'] = $request->role_id ?? null;

        return $this->userRepository->search($dataSearch);
    }

    public function findById($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function store($request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);
        $user = $this->userRepository->create($dataCreate);
        $user->attachRole($request->role_id);
        return $user;
    }

    public function update($request, $id)
    {
        $user = $this->userRepository->findOrFail($id);
        $dataUpdate = $request->except('password');
        if ($request->password) {
            $dataUpdate['password'] = Hash::make($request->password);
        }
        $user->update($dataUpdate);
        $user->syncRole($request->role_id);
        return $user;
    }

    public function destroy($id)
    {
        $user = $this->userRepository->findOrFail($id);
        $user->detachRole();
        $user->delete();
        return $user;
    }
}
