<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        //tra ve la name space
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])

            ->withCategory($dataSearch['category_ids'])
            ->withMinPrice($dataSearch['min_price'])
            ->withMaxPrice($dataSearch['max_price'])
            ->latest('id')->paginate(5);
    }

    public function count()
    {
        return $this->model->count();
    }
}
