<?php

namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{

    public function veryFileImage($request)
    {
        return $request->hasFile('image');
    }

    public function saveImage($request)
    {
        if ($this->veryFileImage($request)) {
            $file = $request->file('image');
            $fileName = time() . $file->getClientOriginalName();
            $filePath = config('image.path');
            $image = Image::make($file);
            $image->fit(150, 150)->save($filePath . '/' . $fileName);
            return $fileName;
        }
        return $this->config('image.imageDefault');
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->veryFileImage($request)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($request);
        }
        return $currentImage;
    }

    public function deleteImage($imageName)
    {
        $pathName = config('image.path') . '/' . $imageName;
        if (!empty($imageName) && file_exists($pathName) && $imageName != config('image.imageDefault')) {
            unlink($pathName);
        }
    }
}
