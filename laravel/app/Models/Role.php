<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name',
        'display_name'
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function hasPermissions($permissionName)
    {
        return $this->permissions->where('name', $permissionName)->toArray();
    }

    public function attachPermission($permissionId)
    {
        return $this->permissions()->attach($permissionId);
    }

    public function detachPermission()
    {
        return $this->permissions()->detach();
    }

    public function syncPermission($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }
}
