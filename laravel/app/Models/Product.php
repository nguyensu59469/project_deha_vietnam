<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'price',
        'quantity',
        'description',
        'image',
    ];

    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function attachCategory($categoryId)
    {
        return $this->categories()->attach($categoryId);
    }

    public function syncCategory($categoryId)
    {
        return $this->categories()->sync($categoryId);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>=', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<=', $maxPrice) : null;
    }

    public function scopeWithCategory($query, $categoryId)
    {
        return $categoryId ? $query->whereHas('categories', function ($query) use ($categoryId) {
            $query->where('category_id', $categoryId);
        }) : null;
    }

    public function getImageAttribute($image)
    {
        return $image ? asset(config('image.path') . '/' . $image) : asset(config('image.imageDefault'));
    }
}
