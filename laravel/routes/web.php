<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');

Route::get('/', [HomeController::class, 'index'])
    ->name('home')
    ->middleware('auth');


//role
Route::prefix('roles')->name('roles.')->middleware('auth')->group(function () {
    Route::get('/', [RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:role_list');

    Route::post('/', [RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:role_create');

    Route::get('/create', [RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:role_create');

    Route::get('/show/{id}', [RoleController::class, 'show'])
        ->name('show')
        ->middleware('permission:role_show');

    Route::get('/edit/{id}', [RoleController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:role_edit');

    Route::put('/update/{id}', [RoleController::class, 'update'])
        ->name('update')
        ->middleware('permission:role_edit');

    Route::delete('/destroy/{id}', [RoleController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:role_delete');

});


//user
Route::name('users.')->prefix('users')->middleware('auth')->group(function () {
    Route::get('/', [UserController::class, 'index'])
        ->name('index')
        ->middleware('permission:user_list');

    Route::post('/', [UserController::class, 'store'])
        ->name('store')
        ->middleware('permission:user_create');

    Route::get('/create', [UserController::class, 'create'])
        ->name('create')
        ->middleware('permission:user_create');

    Route::get('/show/{id}', [UserController::class, 'show'])
        ->name('show')
        ->middleware('permission:user_show');

    Route::get('/edit/{id}', [UserController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:user_edit');

    Route::put('/update/{id}', [UserController::class, 'update'])
        ->name('update')
        ->middleware('permission:user_edit');

    Route::delete('/destroy/{id}', [UserController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:user_delete');

});


//categories
Route::prefix('categories')->name('categories.')->middleware('auth')->group(function () {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('index')
        ->middleware('permission:category_list');

    Route::post('/', [CategoryController::class, 'store'])
        ->name('store')
        ->middleware('permission:category_create');

    Route::get('/create', [CategoryController::class, 'create'])
        ->name('create')
        ->middleware('permission:category_create');

    Route::get('/show/{id}', [CategoryController::class, 'show'])
        ->name('show')
        ->middleware('permission:category_show');

    Route::get('/edit/{id}', [CategoryController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:category_edit');

    Route::put('/update/{id}', [CategoryController::class, 'update'])
        ->name('update')
        ->middleware('permission:category_edit');

    Route::delete('/destroy/{id}', [CategoryController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:category_delete');

});


//products
Route::prefix('products')->name('products.')->middleware('auth')->group(function () {
    Route::get('/', [ProductController::class, 'index'])
        ->name('index')
        ->middleware('permission:product_list');

    Route::post('/', [ProductController::class, 'store'])
        ->name('store')
        ->middleware('permission:product_create');

    Route::get('/list', [ProductController::class, 'list'])
        ->name('list');

    Route::get('/show/{id}', [ProductController::class, 'show'])
        ->name('show')
        ->middleware('permission:product_show');

    Route::get('/create', [ProductController::class, 'create'])
        ->name('create')
        ->middleware('permission:product_create');

    Route::get('/edit/{id}', [ProductController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:product_edit');

    Route::put('/update/{id}', [ProductController::class, 'update'])
        ->name('update')
        ->middleware('permission:product_edit');

    Route::delete('/destroy/{id}', [ProductController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:product_delete');

});
