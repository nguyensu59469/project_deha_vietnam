<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'Nguyen Su',
                'email' => 'admin@gmail.com',
                'address' => 'Ha Noi',
                'phone' => '0339559891',
                'password' => Hash::make('04042002'),
            ]
        );
        DB::table('roles')->insert([
            [
                'name' => 'super-admin',
                'display_name' => 'Super Admin',
            ],
            [
                'name' => 'admin',
                'display_name' => 'Admin',
            ],
            [
                'name' => 'staff',
                'display_name' => 'Staff',
            ],
            [
                'name' => 'guest',
                'display_name' => 'Guest',
            ],
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'user_list',
                'display_name' => 'User List',
                'group_name' => 'User',
            ],
            [
                'name' => 'user_create',
                'display_name' => 'User Create',
                'group_name' => 'User',
            ],
            [
                'name' => 'user_edit',
                'display_name' => 'User Edit',
                'group_name' => 'User',
            ],
            [
                'name' => 'user_show',
                'display_name' => 'User Show',
                'group_name' => 'User',
            ],
            [
                'name' => 'user_delete',
                'display_name' => 'User Delete',
                'group_name' => 'User',
            ],
            [
                'name' => 'role_list',
                'display_name' => 'Role List',
                'group_name' => 'Role',
            ],
            [
                'name' => 'role_show',
                'display_name' => 'Role Show',
                'group_name' => 'Role',
            ],
            [
                'name' => 'role_create',
                'display_name' => 'Role Create',
                'group_name' => 'Role'
            ],
            [
                'name' => 'role_edit',
                'display_name' => 'Role Edit',
                'group_name' => 'Role'
            ],
            [
                'name' => 'role_delete',
                'display_name' => 'Role Delete',
                'group_name' => 'Role'
            ],
            [
                'name' => 'permission_list',
                'display_name' => 'Permission List',
                'group_name' => 'Permission'
            ],
            [
                'name' => 'permission_show',
                'display_name' => 'Permission Show',
                'group_name' => 'Permission'
            ],
            [
                'name' => 'permission_create',
                'display_name' => 'Permission Create',
                'group_name' => 'Permission'
            ],
            [
                'name' => 'permission_edit',
                'display_name' => 'Permission Edit',
                'group_name' => 'Permission'
            ],
            [
                'name' => 'permission_delete',
                'display_name' => 'Permission Delete',
                'group_name' => 'Permission'
            ],
            [
                'name' => 'category_list',
                'display_name' => 'Category List',
                'group_name' => 'Category'
            ],
            [
                'name' => 'category_show',
                'display_name' => 'Category Show',
                'group_name' => 'Category'
            ],
            [
                'name' => 'category_create',
                'display_name' => 'Category Create',
                'group_name' => 'Category'
            ],
            [
                'name' => 'category_edit',
                'display_name' => 'Category Edit',
                'group_name' => 'Category'
            ],
            [
                'name' => 'category_delete',
                'display_name' => 'Category Delete',
                'group_name' => 'Category'
            ],
            [
                'name' => 'product_list',
                'display_name' => 'Product List',
                'group_name' => 'Product'
            ],
            [
                'name' => 'product_create',
                'display_name' => 'Product Create',
                'group_name' => 'Product'

            ],
            [
                'name' => 'product_show',
                'display_name' => 'Product Show',
                'group_name' => 'Product'

            ],
            [
                'name' => 'product_edit',
                'display_name' => 'Product Edit',
                'group_name' => 'Product'
            ],
            [
                'name' => 'product_delete',
                'display_name' => 'Product Delete',
                'group_name' => 'Product'

            ],

        ]);

        DB::table('categories')->insert([
            [
                'name' => 'Laptop',
                'parent_id' => 1,
                'slug' => 'lap-top',
            ],
            [
                'name' => 'Car',
                'parent_id' => 2,
                'slug' => 'car',
            ],
        ]);
        DB::table('role_user')->insert([
            [
                'user_id' => 1,
                'role_id' => 1,
            ],
            [
                'user_id' => 1,
                'role_id' => 2,
            ],
            [
                'user_id' => 1,
                'role_id' => 3,
            ],
        ]);
        DB::table('permission_role')->insert([
            [
                'permission_id' => 1,
                'role_id' => 1,
            ],
            [
                'permission_id' => 2,
                'role_id' => 1,
            ],
            [
                'permission_id' => 3,
                'role_id' => 1,
            ],
            [
                'permission_id' => 4,
                'role_id' => 1,
            ],
            [
                'permission_id' => 5,
                'role_id' => 1,
            ],
            [
                'permission_id' => 6,
                'role_id' => 1,
            ],
            [
                'permission_id' => 7,
                'role_id' => 1,
            ],
            [
                'permission_id' => 8,
                'role_id' => 1,
            ],
            [
                'permission_id' => 9,
                'role_id' => 1,
            ],
            [
                'permission_id' => 10,
                'role_id' => 1,
            ],
            [
                'permission_id' => 11,
                'role_id' => 1,
            ],
            [
                'permission_id' => 12,
                'role_id' => 1,
            ],
            [
                'permission_id' => 13,
                'role_id' => 1,
            ],
            [
                'permission_id' => 14,
                'role_id' => 1,
            ],
            [
                'permission_id' => 15,
                'role_id' => 1,
            ],
            [
                'permission_id' => 16,
                'role_id' => 1,
            ],
            [
                'permission_id' => 17,
                'role_id' => 1,
            ],
            [
                'permission_id' => 18,
                'role_id' => 1,
            ],
            [
                'permission_id' => 19,
                'role_id' => 1,
            ],
            [
                'permission_id' => 20,
                'role_id' => 1,
            ],
            [
                'permission_id' => 21,
                'role_id' => 1,
            ],
            [
                'permission_id' => 22,
                'role_id' => 1,
            ],
            [
                'permission_id' => 23,
                'role_id' => 1,
            ],
            [
                'permission_id' => 24,
                'role_id' => 1,
            ],
            [
                'permission_id' => 25,
                'role_id' => 1,
            ],
            [
                'permission_id' => 1,
                'role_id' => 2,
            ],
            [
                'permission_id' => 2,
                'role_id' => 2,
            ],
            [
                'permission_id' => 3,
                'role_id' => 2,
            ],
            [
                'permission_id' => 4,
                'role_id' => 2,
            ],
            [
                'permission_id' => 13,
                'role_id' => 2,
            ],
            [
                'permission_id' => 14,
                'role_id' => 2,
            ],
            [
                'permission_id' => 15,
                'role_id' => 2,
            ],
            [
                'permission_id' => 16,
                'role_id' => 2,
            ],
            [
                'permission_id' => 17,
                'role_id' => 2,
            ],
            [
                'permission_id' => 18,
                'role_id' => 2,
            ],
            [
                'permission_id' => 19,
                'role_id' => 2,
            ],
            [
                'permission_id' => 20,
                'role_id' => 2,
            ],
            [
                'permission_id' => 13,
                'role_id' => 3,
            ],
            [
                'permission_id' => 14,
                'role_id' => 3,
            ],
            [
                'permission_id' => 15,
                'role_id' => 3,
            ],
            [
                'permission_id' => 16,
                'role_id' => 3,
            ],
            [
                'permission_id' => 17,
                'role_id' => 3,
            ],
            [
                'permission_id' => 18,
                'role_id' => 3,
            ],
            [
                'permission_id' => 19,
                'role_id' => 3,
            ],
            [
                'permission_id' => 20,
                'role_id' => 3,
            ],
        ]);
    }
}
