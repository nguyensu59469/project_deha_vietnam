$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const Base = (function () {
    let modules = {}

    modules.debounce = function (func, timeout = 1000) {
        let timer;
        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                func.apply(this, args);
            }, timeout);
        };

    }

    modules.callApiData = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false,
        })
    }

    modules.callApiNormally = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
        })
    }

    modules.confirmDelete = function (name = 'name') {
        return new Promise((resolve, reject) => {//
            Swal.fire({
                title: 'Are you sure delete ' + name + ' ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                console.log(result);
                if (result.isConfirmed) {
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    }
    return modules;

}(window.jQuery, window, document));

const ImageBase = (function () {
    let modules = {};

    modules.showImage = function (e) {
        let file = e.get(0).files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function () {
                $('.show-image').html('').append('<div class="overlay"> ' +
                    '<button class="text-white bg-danger move-on-hover btn-delete-image" style="z-index: 1">X</button></div>' +
                    '<img src="' + reader.result + '" width="100%">').css('border', '1px solid #eb4b64');
            }
            reader.readAsDataURL(file);
        }
    };

    modules.deleteImage = function () {
        $('.show-image').html('').css('border', '0');
        $('.image').val(null);
    };

    modules.checkImage = function (imgSrc, id) {
        let img = new Image();
        img.onload = function () {
            $('#' + id).attr('src', imgSrc);
        };
        img.onerror = function () {
            $('#' + id).attr('src', 'upload/images/default.jpg');
        };
        img.src = imgSrc;
    }

    return modules;
}(window.jQuery, window, document));

const CustomAlert = (function () {
    let modules = {}

    modules.messageSuccessModal = function (message) {
        Swal.fire({
            position: 'top',
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
    }

    modules.showMessage = function (obj) {
        $.each(obj, function (key, value) {
            $(`.error-${key}`).html(value);
        });
    };

    modules.resetError = function () {
        $('.error').html('');
    };

    modules.showModalError = function (error) {
        Swal.fire({
            icon: 'error',
            title: 'Warning...',
            text: 'Something went wrong!',
        })
    };
    return modules;

}(window.jQuery, window, document));

const Delete = (function () {
    let modules = {}

    modules.delete = function (element) {
        let name = element.data('name-show');
        Base.confirmDelete(name).then(() => {
            $('#delete-' + element.data('name') + '-' + element.data('id')).submit();
        })
    };
    return modules;

}(window.jQuery, window, document));


