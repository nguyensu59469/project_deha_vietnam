const Product = (function () {
    let modules = {}

    modules.getList = function () {
        let url = $('#show-product').attr('data-action-table');
        let formData = $('#search-product').serialize();
        Base.callApiData(url, formData,'GET' ).then(function (res) {
            $('#show-product').html('').append(res);
        });
    }

    modules.getListByPage = function (e) {
        let url = e.attr('href');
        Base.callApiNormally(url).then(function (res) {
            $('#show-product').html('').append(res);
        })
    }

    modules.create = function () {
        let url = $('#addProductForm').data('action');
        let data = new FormData(document.getElementById('addProductForm'));
        CustomAlert.resetError();
        Base.callApiData(url, data, 'POST')
            .then(function (res) {
                $('#modal-form-create').modal('hide');
                Product.resetForm();
                Product.getList();
                CustomAlert.messageSuccessModal(res.message);
            })
            .fail(function (res) {
                CustomAlert.showMessage(JSON.parse(res.responseText).errors);
            });
    }
    modules.show = function () {
        $('#modal-show').modal('show');
    }

    modules.fillToShow = function (product) {
        $('#product-name').text(product.name);
        $('#product-image').attr('src', product.image);
        $('#product-quantity').text(product.quantity);
        $('#product-price').text(product.price);
        $('#product-description').text(product.description);
        ImageBase.checkImage(product.image, 'product-image');
    }

    modules.getCategories = function (res) {
        let categories = "";
        data.categories.forEach(category => {
            categories = category.name + "/ ";
        });
        return categories;
    }

    modules.getShowData = function (element) {
        let url = element.attr('data-url-show');
        Base.callApiNormally(url).then(function (res) {
            Product.fillToShow(res.data);
        })
    }

    modules.update = function () {
        let url = $("#btn-update").attr("data-url");
        let data = new FormData(document.getElementById('updateProductForm'));
        data.append('_method', 'put');
        CustomAlert.resetError();
        Base.callApiData(url, data, 'post')
            .done(function (res) {
                Product.resetForm();
                $('#modal-edit').modal('hide');
                $('.modal-backdrop').remove();
                CustomAlert.messageSuccessModal(res.message);

                Product.getList();
            })
            .fail(function (res) {
                console.log(res)
                CustomAlert.showMessage(JSON.parse(res.responseText).errors);
            });
    }

    modules.delete = function (element) {
        let url = element.data('action');
        let name = element.data('name');
        Base.confirmDelete(name).then(
            function () {
                Base.callApiNormally(url, {}, 'delete')
                    .then(function () {
                        Product.getList($('#list').data('action'));

                        CustomAlert.messageSuccessModal("Delete product success!");
                    })
                    .fail(function () {
                        CustomAlert.showMessage();
                    });
            }
        )
    }

    modules.previewImage = function (e, img) {
        let output = document.getElementById(img);
        output.src = URL.createObjectURL(e.target.files[0]);
        output.setAttribute("style", "max-width: 30%; height: auto;");
        output.onload = function () {
            URL.revokeObjectURL(output.src)
        }
    }

    modules.resetForm = function () {
        $('.selectpicker').val(null).trigger("change");
        $('#addProductForm').trigger('reset');
        $(".show-image").attr("src", "");
    }

    modules.getEditData = function (element) {
        let url = element.attr('data-url-edit');
        Base.callApiNormally(url).then(function (res) {
            $('#title-edit').text("Edit Product: " + res.data.name);
            $('#image-edit').attr('src', res.data.image);
            $('#name-edit').val(res.data.name);
            $('#price-edit').val(res.data.price);
            $('#description-edit').val(res.data.description);
            $('#quantity-edit').val(res.data.quantity);
            $('.btn-update').attr('data-url', res.data.route);
            res.data.categories.forEach(category => {
                $(`#category-edit option[value="${category.category_id}"]`).attr('selected','selected');

            });
            ImageBase.checkImage(res.data.image, 'product-image-edit');
        })
    }

    modules.showEditForm = function () {
        $('#modal-edit').modal('show');
        $('#close-edit').click(function () {
            $('#modal-edit').modal('hide');
            Product.resetForm();
        })
    }
    modules.pageLink = function (element) {
        let url = element.attr('href');
        Product.getList(url);
    }


    return modules;

}(window.jQuery, window, document));

(function ($, window, document) {

    $(document).ready(function () {

        Product.getList();

        $('#select2').select2();


        $(document).on('click', '.page-link', function (e) {
            e.preventDefault();
            Product.pageLink($(this));
        });

        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            Product.getListByPage($(this));
        })


        $(document).on('click', '#btn-create', function (e) {
            e.preventDefault();
            Product.create();
        });

        $(document).on('click', '#btn-add-new-product', function (e) {
            ImageBase.deleteImage();
        });

        $(document).on('change', '.image', function () {
            ImageBase.showImage($(this));
        });

        $(document).on('click', '.btn-delete-image', function (e) {
            e.preventDefault();
            ImageBase.deleteImage();
        });

        $(document).on('change', '#image-edit', function (e) {
            e.preventDefault();
            Product.previewImage(e, 'product-image-edit');
        })


        $(document).on('click', '.btn-delete', function (e) {
            e.preventDefault();
            Product.delete($(this));
        });


        $(document).on('click', '.btn-edit', function (e) {
            Product.getEditData($(this));
            Product.showEditForm($(this));
        });

        $(document).on('click', '#btn-update', function (e) {
            e.preventDefault();
            Product.update();
        });

        $(document).on('click', '.btn-show', function (e) {
            Product.getShowData($(this));
            Product.show();
        });

        $('#close-show').click(function () {
            $('#modal-show').modal('hide');
        })

        $('#search-product').on('keyup', Base.debounce(function () {
            Product.getList();
        }))

        $('.category').on('change', Base.debounce(function () {
            Product.getList();
        }))

    })

}(window.jQuery, window, document));


