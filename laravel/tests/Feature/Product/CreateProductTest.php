<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_see_create_product_view()
    {
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateProductRoute());

        $response->assertStatus(Response::HTTP_FORBIDDEN);

    }

    /** @test */
    public function unauthenticated_super_admin_can_not_see_create_product_view()
    {
        $response = $this->get($this->getCreateProductRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_create_product_view()
    {
        $this->loginUserWithPermission('product_create');
        $response = $this->get($this->getCreateProductRoute());

        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function unauthenticated_user_have_permission_can_not_see_create_product_view()
    {
        $response = $this->get($this->getCreateProductRoute());

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }


    public function getCreateProductRoute()
    {
        return route('products.create');
    }

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function _makeFactoryProduct()
    {
        return Product::factory()->make()->toArray();
    }
}
