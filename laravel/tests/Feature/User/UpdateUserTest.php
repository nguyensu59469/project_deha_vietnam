<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function authenticated_super_admin_can_update_user()
    {
        $this->withExceptionHandling();
        $this->loginWithSuperAdmin();
        $user = User::factory()->create();
        $dataUpdate = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FORBIDDEN);
        $this->assertDatabaseHas('users', $dataUpdate);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_see_edit_user_form()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $response = $this->get($this->getEditUserRoute($user->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.edit');
        $response->assertSee(['name'])->assertSee(['email']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_update_user()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users', $dataUpdate);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_user_name_address_phone_address_and_email_are_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = [
            'name' => null,
            'email' => null,
            'phone' => null,
            'address' => null,
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertSessionHasErrors(['name', 'email', 'phone', 'address']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_user_name_is_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = [
            'name' => null,
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_user_email_is_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = [
            'email' => null,
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_user_address_is_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = [
            'address' => null,
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertSessionHasErrors('address');
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_update_user_phone_is_null()
    {
        $this->loginUserWithPermission('user_edit');
        $user = User::factory()->create();
        $dataUpdate = [
            'phone' => null,
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);

        $response->assertSessionHasErrors('phone');
    }

    /** @test */
    public function unauthenticated_user_have_permission_can_not_see_edit_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getEditUserRoute($user->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function getEditUserRoute($id)
    {
        return route('users.edit', ['id' => $id]);
    }

    public function getUpdateUserRoute($id)
    {
        return route('users.update',['id' => $id]);
    }

    public function createFactoryUser()
    {
        return User::factory()->create()->toArray();
    }
}
