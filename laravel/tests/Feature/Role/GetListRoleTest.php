<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRoleTest extends TestCase
{
    /** @test */
    public function authenticated_user_have_permission_can_get_all_roles()
    {
        $this->loginUserWithPermission('role_list');
        $response = $this->get($this->getListRoleRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.roles.index');
    }

    /** @test  */
    public function unauthenticated_user_can_not_get_all_roles()
    {
        $response =  $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function getListRoleRoute()
    {
        return route('roles.index');
    }
}
