<?php

namespace Tests\Feature\Auths;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;


class RegisterTest extends TestCase
{

    /** @test */
    public function user_can_register_if_data_is_valid()
    {
        $dataRegister = [
            'name' => 'name',
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $userCountBefore = User::count();
        $response = $this->post(route('register', $dataRegister));
        $userCountAfter = User::count();
        $response->assertStatus(Response::HTTP_FOUND);
        array_splice($dataRegister, 2);
        $response->assertRedirect(route('home'));

    }

    /** @test */
    public function user_can_not_register_if_email_is_exist()
    {
        $user = User::factory()->create();
        $dataRegister = [
            'name' => 'name',
            'email' => $user->email,
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_not_register_if_password_is_not_valid()
    {
        $dataRegister = [
            'name' => 'name',
            'email' => '@deha-soft.com',
            'password' => '123',
            'password_confirmation' => '123'
        ];
        $response = $this->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function user_can_not_register_if_name_filed_is_null()
    {
        $dataRegister = [
            'name' => null,
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function user_can_not_register_if_email_filed_is_null()
    {
        $dataRegister = [
            'name' => 'name',
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_not_register_if_password_filed_is_null()
    {
        $dataRegister = [
            'name' => 'name',
            'email' => '@deha-soft.com',
            'password' => null,
            'password_confirmation' => '123456789'
        ];
        $response = $this->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }


    /** @test */
    public function user_can_see_view_register()
    {
        $response = $this->get(route('register'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.register');
        $response->assertSeeText('Register');
    }

    /** @test */
    public function user_can_see_name_required_text_if_validate_error()
    {
        $dataRegister = [
            'name' => null,
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function user_can_see_email_required_text_if_validate_error()
    {
        $dataRegister = [
            'name' =>'name',
            'email' => null,
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_see_password_required_text_if_validate_error()
    {
        $dataRegister = [
            'name' => null,
            'email' => '@deha-soft.com',
            'password' => null,
            'password_confirmation' => '123456789'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function user_can_see_email_max_255_text_if_validate_error()
    {
        $dataRegister = [
            'name' =>'name',
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => '123456789'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_see_password_confirm_text_if_validate_error()
    {
        $dataRegister = [
            'name' =>'name',
            'email' => '@deha-soft.com',
            'password' => '123456789',
            'password_confirmation' => 'khonggiongnhau'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('password');
    }

    /** @test */
    public function user_can_see_password_of_at_least_8_text_if_validate_error()
    {
        $dataRegister = [
            'name' =>'name',
            'email' => 'email',
            'password' => '123456',
            'password_confirmation' => '123456789'
        ];
        $response = $this->from(route('register'))->post(route('register', $dataRegister));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('register'));
        $response->assertSessionHasErrors('password');
    }

}
