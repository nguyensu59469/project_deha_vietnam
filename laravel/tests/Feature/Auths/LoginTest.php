<?php

namespace Tests\Feature\Auths;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{

    /** @test */
    public function user_can_login_if_account_is_exist()
    {
        $userCreated = User::factory()->create(['password' => bcrypt('123456789')]);
        $user = [
            'email' => $userCreated->email,
            'password' => '123456789'
        ];
        $response = $this->post(route('login', $user));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/home');
    }

    /** @test */
    public function user_can_not_login_if_account_password_wrong()
    {
        $userCreated = User::factory()->create(['password' => bcrypt('123456789')]);
        $user = [
            'email' => $userCreated->email,
            'password' => '1234567'
        ];
        $response = $this->post(route('login', $user));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_not_login_if_account_is_not_email()
    {
        $user = User::factory()->make(['email'=>'abc@gmail', 'password' => bcrypt('123456789')])->toArray();
        $response = $this->post(route('login', $user));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function user_can_see_view_login()
    {
        $response = $this->get(route('login'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.login');
    }

}
